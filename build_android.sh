rm -f *.tmp
rm -f AATKitDemo.swf
rm -f AATKitDemo.apk

amxmlc -load-config config-android.xml AATKitDemo.as

echo " "
echo "Building android application ..."
adt -package -target apk-debug -storetype pkcs12 -keystore intentsoftware.p12 -storepass intent2015 AATKitDemo.apk AATKitDemo-android.xml -extdir extensions AATKitDemo.swf -C assets .

if [ $# -ne 0 ];
then
	echo " "
	echo "Installing apk on device ..."
	adb install -r AATKitDemo.apk
fi
