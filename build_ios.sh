rm -rf *.tmp
rm -f AATKitDemo.swf
rm -f AATKitDemo.ipa
rm -rf AATKitDemo.app.dSYM

amxmlc -load-config config-ios.xml AATKitDemo.as

echo "Building ipa file ..."
adt -package -target ipa-debug -keystore intentsoftwareDev.p12 -storetype pkcs12 -storepass intent2017 -provisioning-profile generic.mobileprovision AATKitDemo.ipa AATKitDemo-ios.xml -extdir extensions AATKitDemo.swf Default~iphone.png Default@2x~iphone.png Default-568h@2x~iphone.png Default-375w-667h@2x~iphone.png Default-414w-736h@3x~iphone.png

if [ $# -ne 0 ];
then
	echo "Installing on device ..."
	handle="$(adt -devices -platform ios | head -3 | tail -1 | cut -c1-5 | xargs)"
	adt -installApp -platform ios -device "${handle}" -package AATKitDemo.ipa
	echo "Done.."
fi
