# AATKit AdobeAIR Sample App


## Overview

The aatkit plugin for Adobe AIR offers easy integration of mobile ad networks. AddApptr gives you access to all the networks with one single SDK.

## Installation

#### 1. Clone this repository.
#### 2. Download [AATKit_AdobeAIR.zip](http://isxfs.com/gecko/AATKit_AdobeAIR/AATKit_AdobeAIR.zip) and extract plugin
#### 3. Copy all .ANE Extension files into adobe-air-plugin-sample-app/extensions.
#### 4. Copy all folders with assets files into adobe-air-plugin-sample-app/assets.
#### 5a. To build Android sample application run build_android.sh.
#### 5b. To build iOS sample application run build_ios.sh.

## ---------------------
##  [Plugin documentation](https://bitbucket.org/dbrockhaus/adobe-air-plugin/wiki/Home)