package
{
	import com.intentsoftware.addapptr.AATKitExtension;
	import com.intentsoftware.addapptr.AATKitConfiguration;
	import com.intentsoftware.addapptr.AATKitEvent;

	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.display.DisplayObjectContainer;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.utils.Dictionary;

	public class AATKitDemo extends Sprite
	{
		private var aatkit:AATKitExtension = null;
		private var version:String = "unknown";
		private var bannerId:String = "BannerPlacement";
		private var multiSizeBannerId:String = "MultiSizePlacement";
		private var interstitialId:String = "RewardedVideo";
		private var initialized:Boolean = false;
		private var shakeEnabled:Boolean = false;
		private var bannerAutoreload:Boolean = false;
		private var interstitialAutoreload:Boolean = false;
		private var bannerAlignment:int = AATKitExtension.BOTTOMCENTER;

		public function aatButton(label, posy):SimpleButton
		{
			var aatLabel:TextField = new TextField();
			aatLabel.name = "label";
			aatLabel.defaultTextFormat = new TextFormat('Verdana', 24, 0xffffff);
			aatLabel.autoSize = TextFieldAutoSize.LEFT;
			aatLabel.x = 30;
			aatLabel.y = posy+6;
			aatLabel.text = label;
			aatLabel.mouseEnabled = false;

			var aatSprite:Sprite = new Sprite();
			aatSprite.graphics.beginFill(0xff0000, 1);
			aatSprite.graphics.drawRect(20, posy, 450, 48);
			aatSprite.graphics.endFill();
			aatSprite.addChild(aatLabel);

			var aatLabel2:TextField = new TextField();
			aatLabel.name = "label2";
			aatLabel2.defaultTextFormat = new TextFormat('Verdana', 24, 0xffffff);
			aatLabel2.autoSize = TextFieldAutoSize.LEFT;
			aatLabel2.x = 30;
			aatLabel2.y = posy+6;
			aatLabel2.text = label;
			aatLabel2.mouseEnabled = false;

			var aatSpriteClick:Sprite = new Sprite();
			aatSpriteClick.graphics.beginFill(0x00ff00, 1);
			aatSpriteClick.graphics.drawRect(20, posy, 450, 48);
			aatSpriteClick.graphics.endFill();
			aatSpriteClick.addChild(aatLabel2);

			var aatButton:SimpleButton = new SimpleButton();
			aatButton.overState = aatButton.upState = aatButton.hitTestState = aatSprite;
			aatButton.downState = aatSpriteClick;

			addChild(aatButton);

			return aatButton;
		}

		public function AATKitDemo()
		{
			super();

			aatkit = AATKitExtension.getInstance();

			version = aatkit.getVersion();
			var versionLabel:TextField = new TextField();
			versionLabel.defaultTextFormat = new TextFormat('Verdana', 16, 0x000000);
			versionLabel.autoSize = TextFieldAutoSize.LEFT;
			versionLabel.x = 20;
			versionLabel.y = -164;
			versionLabel.text = version;
			versionLabel.mouseEnabled = false;
			addChild(versionLabel);

			var initBtn:SimpleButton = aatButton("Init AATKit", -140);
			var debugBtn:SimpleButton = aatButton("Enable Debug Log", -85);
			var shakeBtn:SimpleButton = aatButton("Enable/Disable Debug Shake", -30);

			var reloadBannerBtn:SimpleButton = aatButton("Reload Banner", 25);
			var autoreloadBannerBtn:SimpleButton = aatButton("Enable/Disable Banner Autoreload", 80);
			var alignBannerBtn:SimpleButton = aatButton("Change Banner Alignment", 135);

			var reloadMultisizeBannerBtn:SimpleButton = aatButton("Reload Multisize Banner", 190);
			var autoreloadMultisizeBannerBtn:SimpleButton = aatButton("Enable/Disable Multisize Banner Autoreload", 245);
			var alignMultisizeBannerBtn:SimpleButton = aatButton("Change Multisize Banner Alignment", 300);

			var reloadInterstitialBtn:SimpleButton = aatButton("Reload Interstitial", 355);
			var autoreloadInterstitialBtn:SimpleButton = aatButton("Enable/Disable Interstitial Autoreload", 410);
			var showInterstitialBtn:SimpleButton = aatButton("Show Interstitial", 465);

			initBtn.addEventListener(MouseEvent.CLICK, initListener);
			function initListener(e:MouseEvent):void
			{
				if(!initialized)
				{
					var configuration:AATKitConfiguration = new AATKitConfiguration();
					configuration.testModeAccountID = 74;
					configuration.consentRequired = true;
					configuration.simpleConsent = AATKitConfiguration.OBTAINED;
					aatkit.initWithConfiguration(configuration);

					aatkit.createPlacement(bannerId, AATKitExtension.BANNERAUTO);
					aatkit.createPlacement(multiSizeBannerId, AATKitExtension.MULTISIZEBANNER);
					aatkit.createPlacement(interstitialId, AATKitExtension.FULLSCREEN);

					initialized = true;
				}
			}

			debugBtn.addEventListener(MouseEvent.CLICK, debugListener);
			function debugListener(e:MouseEvent):void
			{
				aatkit.setDebugEnabled();
			}

			shakeBtn.addEventListener(MouseEvent.CLICK, shakeListener);
			function shakeListener(e:MouseEvent):void
			{
				if(initialized)
				{
					if(shakeEnabled)
					{
						aatkit.setDebugShakeEnabled(false);
						shakeEnabled = false;
					}
					else
					{
						aatkit.setDebugShakeEnabled(true);
						shakeEnabled = true;
					}
				}
			}

			//
			reloadBannerBtn.addEventListener(MouseEvent.CLICK, reloadBannerListener);
			function reloadBannerListener(e:MouseEvent):void
			{
				if(initialized)
				{
					aatkit.reloadPlacement(bannerId);

					var dictionary:Dictionary = new Dictionary();
					dictionary["key1"] = new Array("value1", "value2");
					dictionary["key2"] = new Array("value3", "value4");

					aatkit.setTargetingInfo(dictionary);
				}
			}

			autoreloadBannerBtn.addEventListener(MouseEvent.CLICK, autoreloadBannerListener);
			function autoreloadBannerListener(e:MouseEvent):void
			{
				if(initialized)
				{
					if(bannerAutoreload)
					{
						aatkit.stopPlacementAutoReload(bannerId);
						bannerAutoreload = false;
					}
					else
					{
						aatkit.startPlacementAutoReload(bannerId);
						bannerAutoreload = true;
					}
				}
			}

			alignBannerBtn.addEventListener(MouseEvent.CLICK, alignBannerListener);
			function alignBannerListener(e:MouseEvent):void
			{
				if(initialized)
				{
					switch(bannerAlignment)
					{
						case AATKitExtension.BOTTOMCENTER:
							aatkit.setPlacementAlignment(bannerId, AATKitExtension.BOTTOMRIGHT);
							bannerAlignment = AATKitExtension.BOTTOMRIGHT;
							break;
						case AATKitExtension.BOTTOMRIGHT:
							aatkit.setPlacementAlignment(bannerId, AATKitExtension.BOTTOMLEFT);
							bannerAlignment = AATKitExtension.BOTTOMLEFT;
							break;
						case AATKitExtension.BOTTOMLEFT:
							aatkit.setPlacementAlignment(bannerId, AATKitExtension.TOPCENTER);
							bannerAlignment = AATKitExtension.TOPCENTER;
							break;
						case AATKitExtension.TOPCENTER:
							aatkit.setPlacementAlignment(bannerId, AATKitExtension.TOPRIGHT);
							bannerAlignment = AATKitExtension.TOPRIGHT;
							break;
						case AATKitExtension.TOPRIGHT:
							aatkit.setPlacementAlignment(bannerId, AATKitExtension.TOPLEFT);
							bannerAlignment = AATKitExtension.TOPLEFT;
							break;
						case AATKitExtension.TOPLEFT:
							aatkit.setPlacementAlignment(bannerId, AATKitExtension.BOTTOMCENTER);
							bannerAlignment = AATKitExtension.BOTTOMCENTER;
							break;
					}
				}
			}

			//
			reloadMultisizeBannerBtn.addEventListener(MouseEvent.CLICK, reloadMultisizeBannerListener);
			function reloadMultisizeBannerListener(e:MouseEvent):void
			{
				if(initialized)
				{
					aatkit.reloadPlacement(multiSizeBannerId);
				}
			}

			autoreloadMultisizeBannerBtn.addEventListener(MouseEvent.CLICK, autoreloadMultisizeBannerListener);
			function autoreloadMultisizeBannerListener(e:MouseEvent):void
			{
				if(initialized)
				{
					if(bannerAutoreload)
					{
						aatkit.stopPlacementAutoReload(multiSizeBannerId);
						bannerAutoreload = false;
					}
					else
					{
						aatkit.startPlacementAutoReload(multiSizeBannerId);
						bannerAutoreload = true;
					}
				}
			}

			alignMultisizeBannerBtn.addEventListener(MouseEvent.CLICK, alignMultisizeBannerListener);
			function alignMultisizeBannerListener(e:MouseEvent):void
			{
				if(initialized)
				{
					switch(bannerAlignment)
					{
						case AATKitExtension.BOTTOMCENTER:
							aatkit.setPlacementAlignment(multiSizeBannerId, AATKitExtension.BOTTOMRIGHT);
							bannerAlignment = AATKitExtension.BOTTOMRIGHT;
							break;
						case AATKitExtension.BOTTOMRIGHT:
							aatkit.setPlacementAlignment(multiSizeBannerId, AATKitExtension.BOTTOMLEFT);
							bannerAlignment = AATKitExtension.BOTTOMLEFT;
							break;
						case AATKitExtension.BOTTOMLEFT:
							aatkit.setPlacementAlignment(multiSizeBannerId, AATKitExtension.TOPCENTER);
							bannerAlignment = AATKitExtension.TOPCENTER;
							break;
						case AATKitExtension.TOPCENTER:
							aatkit.setPlacementAlignment(multiSizeBannerId, AATKitExtension.TOPRIGHT);
							bannerAlignment = AATKitExtension.TOPRIGHT;
							break;
						case AATKitExtension.TOPRIGHT:
							aatkit.setPlacementAlignment(multiSizeBannerId, AATKitExtension.TOPLEFT);
							bannerAlignment = AATKitExtension.TOPLEFT;
							break;
						case AATKitExtension.TOPLEFT:
							aatkit.setPlacementAlignment(multiSizeBannerId, AATKitExtension.BOTTOMCENTER);
							bannerAlignment = AATKitExtension.BOTTOMCENTER;
							break;
					}
				}
			}

			//
			reloadInterstitialBtn.addEventListener(MouseEvent.CLICK, reloadInterstitialListener);
			function reloadInterstitialListener(e:MouseEvent):void
			{
				if(initialized)
				{
					aatkit.reloadPlacement(interstitialId);
				}
			}

			autoreloadInterstitialBtn.addEventListener(MouseEvent.CLICK, autoreloadInterstitialListener);
			function autoreloadInterstitialListener(e:MouseEvent):void
			{
				if(initialized)
				{
					if(interstitialAutoreload)
					{
						aatkit.stopPlacementAutoReload(interstitialId);
						interstitialAutoreload = false;
					}
					else
					{
						aatkit.startPlacementAutoReload(interstitialId);
						interstitialAutoreload = true;
					}
				}
			}

			showInterstitialBtn.addEventListener(MouseEvent.CLICK, showInterstitialListener);
			function showInterstitialListener(e:MouseEvent):void
			{
				if(initialized)
				{
					var shown:Boolean = aatkit.showPlacement(interstitialId);
				}
			}


			// AATKit Events
			aatkit.addEventListener(AATKitEvent.HAVEAD, onHaveAd);
			aatkit.addEventListener(AATKitEvent.NOAD, onNoAd);
			aatkit.addEventListener(AATKitEvent.PAUSEFORAD, onPauseForAd);
			aatkit.addEventListener(AATKitEvent.RESUMEAFTERD, onResumeAfterAd);
			aatkit.addEventListener(AATKitEvent.SHOWINGEMPTY, onShowingEmpty);
			aatkit.addEventListener(AATKitEvent.USEREARNEDINCENTIVE, onUserEarnedIncentive);

			function onHaveAd(event:AATKitEvent):void
			{
				trace("onHaveAd: " + event.placementName);
			}

			function onNoAd(event:AATKitEvent):void
			{
				trace("onNoAd: " + event.placementName);
			}

			function onPauseForAd(event:AATKitEvent):void
			{
				trace("onPauseForAd: " + event.placementName);
			}

			function onResumeAfterAd(event:AATKitEvent):void
			{
				trace("onResumeAfterAd: " + event.placementName);
			}

			function onShowingEmpty(event:AATKitEvent):void
			{
				trace("onShowingEmpty: " + event.placementName);
			}

			function onUserEarnedIncentive(event:AATKitEvent):void
			{
				trace("onUserEarnedIncentive: " + event.placementName);
			}
		}
	}
}
